package com.kppassion.pathtoseniorandroiddev;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.kppassion.pathtoseniorandroiddev.Animation.MyAnimationActivity;
import com.kppassion.pathtoseniorandroiddev.ButterKnief.ButterDemoActivity;
import com.kppassion.pathtoseniorandroiddev.MVP.ui.appgate.AppGateActivity;
import com.kppassion.pathtoseniorandroiddev.RxAndroid.RxAndroidDemoActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.lblButterKnief)
    TextView mLblButterKnief;
    @BindView(R.id.lblRxAndroid)
    TextView mLblRxAndroid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Log.d("MainActivity", "onCreated()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MainActivity", "onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MainActivity", "onResume()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity", "onStop()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MainActivity", "onPause()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MainActivity", "onDestroy()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("MainActivity", "onRestarted()");
    }

    @OnClick({R.id.lblButterKnief, R.id.lblRxAndroid, R.id.lblRxAnimation, R.id.lblMvp})
    void lblClick(View view) {
        switch (view.getId()) {
            case R.id.lblButterKnief:
                startActivity(new Intent(getApplicationContext(), ButterDemoActivity.class));
                break;
            case R.id.lblRxAndroid:
                startActivity(new Intent(getApplicationContext(), RxAndroidDemoActivity.class));
                break;
            case R.id.lblRxAnimation:
                startActivity(new Intent(getApplicationContext(), MyAnimationActivity.class));
                break;
            case R.id.lblMvp:
                startActivity(new Intent(getApplicationContext(), AppGateActivity.class));
                break;
            default:
                break;
        }
    }
}
