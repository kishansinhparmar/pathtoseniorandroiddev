package com.kppassion.pathtoseniorandroiddev.RxAndroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.kppassion.pathtoseniorandroiddev.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class RxAndroidDemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rx_android_demo);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.lblExample1, R.id.lblExample2, R.id.lblExample3})
    public void Example1(View view) {
        switch (view.getId()) {
            case R.id.lblExample1:
                startActivity(new Intent(getApplicationContext(), Example1Activity.class));
                break;
            case R.id.lblExample2:
                startActivity(new Intent(getApplicationContext(), Example2Activity.class));
                break;
            case R.id.lblExample3:
                startActivity(new Intent(getApplicationContext(), TestRxActivity.class));
                break;
            default:
                break;
        }

    }
}
