package com.kppassion.pathtoseniorandroiddev.MVP.ui.usage_of_base;

import com.kppassion.pathtoseniorandroiddev.MVP.ui.base.MvpPresenter;

/**
 * Created by kishansinh on 1/3/18.
 */

public interface ProfilePresenterView<V extends ProfileView> extends MvpPresenter<V> {

    void logout();

    String getEmail();
}
