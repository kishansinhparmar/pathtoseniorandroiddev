package com.kppassion.pathtoseniorandroiddev.MVP.ui.appgate;

/**
 * Created by kishansinh on 28/2/18.
 */

public interface AppGateView {

    public void openDashboard();

    public void openLogin();
}
