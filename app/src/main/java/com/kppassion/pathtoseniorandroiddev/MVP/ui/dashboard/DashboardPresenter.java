package com.kppassion.pathtoseniorandroiddev.MVP.ui.dashboard;

import com.kppassion.pathtoseniorandroiddev.MVP.data.DataManager;

/**
 * Created by kishansinh on 28/2/18.
 */

public class DashboardPresenter implements DashboardPresenterView {

    DataManager mDataManager;
    DashboardView mDashboardView;

    public DashboardPresenter(DataManager dataManager, DashboardView dashboardView) {
        mDataManager = dataManager;
        mDashboardView = dashboardView;
    }

    @Override
    public String getEmail() {
        return mDataManager.getEmailId();
    }

    @Override
    public void setUserLoggedOut() {
        mDataManager.clear();
        mDashboardView.openAppGate();
    }
}
