package com.kppassion.pathtoseniorandroiddev.MVP.ui.usage_of_base;

import com.kppassion.pathtoseniorandroiddev.MVP.ui.base.MvpView;

/**
 * Created by kishansinh on 1/3/18.
 */

public interface ProfileView extends MvpView {

    void showEmail();

    void openLogin();

}
