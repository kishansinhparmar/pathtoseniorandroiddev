package com.kppassion.pathtoseniorandroiddev.MVP.ui.login;

import com.kppassion.pathtoseniorandroiddev.MVP.data.DataManager;
import com.kppassion.pathtoseniorandroiddev.MVP.data.SharedPrefHelper;

/**
 * Created by kishansinh on 28/2/18.
 */

public class LoginPresenter implements LoginPresenterView {

    DataManager mDataManager;
    SharedPrefHelper mSharedPrefHelper;
    LoginView mLoginView;

    public LoginPresenter(DataManager dataManager, LoginView loginView) {
        mDataManager = dataManager;
        mLoginView = loginView;
    }

    @Override
    public void isEmailPwCorrect(String email) {
        mDataManager.saveEmailId(email);
        mDataManager.setLoggedIn();
        mLoginView.openDashboard();
    }
}
