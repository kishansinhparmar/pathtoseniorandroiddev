package com.kppassion.pathtoseniorandroiddev.MVP.ui.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.kppassion.pathtoseniorandroiddev.MVP.data.DataManager;
import com.kppassion.pathtoseniorandroiddev.MVP.data.SharedPrefHelper;
import com.kppassion.pathtoseniorandroiddev.MVP.ui.appgate.AppGateActivity;
import com.kppassion.pathtoseniorandroiddev.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DashboardActivity extends AppCompatActivity implements DashboardView {

    private DataManager mDataManager;
    private DashboardPresenter mPresenter;
    @BindView(R.id.dashboard_lblEmail)
    public TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        mDataManager = new DataManager(new SharedPrefHelper(getApplicationContext()));
        mPresenter = new DashboardPresenter(mDataManager, this);

        mTextView.setText(mPresenter.getEmail());
    }

    @OnClick(R.id.dashboard_btnLogout)
    public void logout() {
        mPresenter.setUserLoggedOut();
    }

    @Override
    public void openAppGate() {
        startActivity(new Intent(getApplicationContext(), AppGateActivity.class));
        finish();
    }
}