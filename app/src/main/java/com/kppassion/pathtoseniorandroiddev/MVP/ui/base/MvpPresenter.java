package com.kppassion.pathtoseniorandroiddev.MVP.ui.base;

/**
 * Created by kishansinh on 28/2/18.
 */

public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);
    
}
