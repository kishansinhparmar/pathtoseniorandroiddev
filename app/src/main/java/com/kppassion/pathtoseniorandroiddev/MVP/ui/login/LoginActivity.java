package com.kppassion.pathtoseniorandroiddev.MVP.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.kppassion.pathtoseniorandroiddev.MVP.CommonUtils.CommonUtils;
import com.kppassion.pathtoseniorandroiddev.MVP.data.DataManager;
import com.kppassion.pathtoseniorandroiddev.MVP.data.SharedPrefHelper;
import com.kppassion.pathtoseniorandroiddev.MVP.ui.dashboard.DashboardActivity;
import com.kppassion.pathtoseniorandroiddev.MVP.ui.usage_of_base.ProfileActivity;
import com.kppassion.pathtoseniorandroiddev.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginView {

    LoginPresenter mLoginPresenter;
    DataManager mDataManager;
    @BindView(R.id.login_edtLogin)
    public EditText mEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mDataManager = new DataManager(new SharedPrefHelper(getApplicationContext()));

        mLoginPresenter = new LoginPresenter(mDataManager, this);
    }

    @Override
    public void openDashboard() {
        startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
        finish();
    }

    @Override
    @OnClick(R.id.login_btnLogin)
    public void onLoginClick() {

        if (!CommonUtils.isEmailValid(mEditText.getText().toString())) {
            Toast.makeText(this, "Enter correct Email", Toast.LENGTH_LONG).show();
            return;
        }

        mLoginPresenter.isEmailPwCorrect(mEditText.getText().toString());
    }
}
