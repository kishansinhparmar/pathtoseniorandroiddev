package com.kppassion.pathtoseniorandroiddev.MVP.ui.usage_of_base;

import com.kppassion.pathtoseniorandroiddev.MVP.data.DataManager;
import com.kppassion.pathtoseniorandroiddev.MVP.ui.base.BasePresenter;

/**
 * Created by kishansinh on 1/3/18.
 */

public class ProfilePresenter<V extends ProfileView> extends BasePresenter<V> implements
        ProfilePresenterView<V> {

    public ProfilePresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void logout() {
        getDataManager().clear();
        getMvpView().openLogin();
    }

    @Override
    public String getEmail() {
        return getDataManager().getEmailId();
    }
}
