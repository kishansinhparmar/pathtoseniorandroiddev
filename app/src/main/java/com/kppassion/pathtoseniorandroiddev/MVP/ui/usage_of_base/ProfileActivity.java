package com.kppassion.pathtoseniorandroiddev.MVP.ui.usage_of_base;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.kppassion.pathtoseniorandroiddev.MVP.data.DataManager;
import com.kppassion.pathtoseniorandroiddev.MVP.data.SharedPrefHelper;
import com.kppassion.pathtoseniorandroiddev.MVP.ui.base.BaseActivity;
import com.kppassion.pathtoseniorandroiddev.MVP.ui.login.LoginActivity;
import com.kppassion.pathtoseniorandroiddev.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends BaseActivity implements ProfileView {

    private DataManager mDataManager;
    private ProfilePresenter mPresenter;
    @BindView(R.id.profile_lblEmail)
    public TextView lblEmail;
    @BindView(R.id.profile_btnLogout)
    public Button btnLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        mDataManager = new DataManager(new SharedPrefHelper(getApplicationContext()));

        mPresenter = new ProfilePresenter(mDataManager);

        mPresenter.onAttach(this);

        showEmail();
    }

    @OnClick(R.id.profile_btnLogout)
    public void logout() {
        mPresenter.logout();
    }

    @Override
    public void showEmail() {
        lblEmail.setText(mPresenter.getEmail());
    }

    @Override
    public void openLogin() {
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }

}
