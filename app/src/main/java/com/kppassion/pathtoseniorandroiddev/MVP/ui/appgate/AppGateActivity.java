package com.kppassion.pathtoseniorandroiddev.MVP.ui.appgate;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.kppassion.pathtoseniorandroiddev.MVP.data.DataManager;
import com.kppassion.pathtoseniorandroiddev.MVP.data.SharedPrefHelper;
import com.kppassion.pathtoseniorandroiddev.MVP.ui.dashboard.DashboardActivity;
import com.kppassion.pathtoseniorandroiddev.MVP.ui.login.LoginActivity;
import com.kppassion.pathtoseniorandroiddev.MVP.ui.usage_of_base.ProfileActivity;
import com.kppassion.pathtoseniorandroiddev.R;

public class AppGateActivity extends AppCompatActivity implements AppGateView {

    private DataManager mDataManager;
    private AppGatePresenter mAppGatePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_gate);

        mDataManager = new DataManager(new SharedPrefHelper(getApplicationContext()));
        
        mAppGatePresenter = new AppGatePresenter(mDataManager,this);

        mAppGatePresenter.loginOrNot();
    }

    @Override
    public void openDashboard() {
        startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
        finish();
    }

    @Override
    public void openLogin() {
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }
}
