package com.kppassion.pathtoseniorandroiddev.MVP.ui.base;

import com.kppassion.pathtoseniorandroiddev.MVP.data.DataManager;

/**
 * Created by kishansinh on 28/2/18.
 */

public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    DataManager mDataManager;
    private V mMvpView;

    public BasePresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    public V getMvpView() {
        return mMvpView;
    }

    public DataManager getDataManager() {
        return mDataManager;
    }
}
