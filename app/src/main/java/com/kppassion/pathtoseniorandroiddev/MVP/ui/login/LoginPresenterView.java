package com.kppassion.pathtoseniorandroiddev.MVP.ui.login;

/**
 * Created by kishansinh on 28/2/18.
 */

public interface LoginPresenterView {

    void isEmailPwCorrect(String email);
}
