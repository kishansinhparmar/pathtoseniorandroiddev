package com.kppassion.pathtoseniorandroiddev.MVP.ui.appgate;

import com.kppassion.pathtoseniorandroiddev.MVP.data.DataManager;

/**
 * Created by kishansinh on 28/2/18.
 */

public class AppGatePresenter implements AppGateMvpPresenter {

    private DataManager mDataManager;
    private AppGateView mAppGateView;

    AppGatePresenter(DataManager dataManager,AppGateView appGateView) {
        mDataManager = dataManager;
        mAppGateView = appGateView;
    }

    @Override
    public void loginOrNot() {
        if (mDataManager.getLoggedInMode()) {
            mAppGateView.openDashboard();
        } else {
            mAppGateView.openLogin();
        }
    }
}
