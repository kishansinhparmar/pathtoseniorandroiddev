package com.kppassion.pathtoseniorandroiddev.Animation;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.kppassion.pathtoseniorandroiddev.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyAnimationActivity extends AppCompatActivity {

    @BindView(R.id.lottieAnimationView)
    public LottieAnimationView mLottieAnimationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_animation);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.lottieAnimationView)
    public void showAnim() {
        ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f).setDuration(1000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                mLottieAnimationView.setProgress((Float) valueAnimator.getAnimatedValue());
            }
        });

        if (mLottieAnimationView.getProgress() == 0f) {
            animator.start();
        } else {
            mLottieAnimationView.setProgress(0f);
        }
    }
}
