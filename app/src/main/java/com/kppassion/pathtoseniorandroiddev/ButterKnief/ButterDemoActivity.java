package com.kppassion.pathtoseniorandroiddev.ButterKnief;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.kppassion.pathtoseniorandroiddev.R;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class ButterDemoActivity extends AppCompatActivity {

    @BindView(R.id.butter_edtName)
    EditText mEditText;
    @BindView(R.id.butter_btnName)
    Button mButton;
    @BindView(R.id.butter_lblName)
    TextView mTextView;
    @BindView(R.id.butter_chkName)
    CheckBox mCheckBox;
    @BindView(R.id.butter_rdName)
    RadioButton mRadioButton;
    @BindString(R.string.app_name)
    String appName;
    @BindViews({R.id.chkAmerica, R.id.chkBritain, R.id.chkChina, R.id.chkIndia})
    CheckBox[] countries;
    @BindView(R.id.butter_btnCheckAll)
    Button mButton2;

    final ButterKnife.Action<CheckBox> CHECK_ALL = new ButterKnife.Action<CheckBox>() {
        @Override
        public void apply(@NonNull CheckBox view, int index) {
            view.setChecked(true);
        }
    };

    final ButterKnife.Setter<CheckBox, Boolean> CHECKED =
            new ButterKnife.Setter<CheckBox, Boolean>() {
                @Override
                public void set(@NonNull CheckBox view, Boolean value, int index) {
                    view.setChecked(value != null ? value : false);
                }
            };
    @BindView(R.id.butter_btnUnCheckAll)
    Button mButton3;
    @BindView(R.id.butter_btnFade)
    Button mButton4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_butter_demo);
        //Bind butter knife here
        ButterKnife.bind(this);

        mTextView.setText(appName);

    }

    @OnClick(R.id.butter_btnName)
    void show() {
        mTextView.setText(mEditText.getText().toString());
    }

    @OnCheckedChanged(R.id.butter_chkName)
    void change() {
        mCheckBox.setText(mCheckBox.isChecked() ? "Checked" : "Uncheck");
    }

    @OnClick(R.id.butter_btnCheckAll)
    public void checkAllFun() {
        ButterKnife.apply(countries, CHECK_ALL);
    }

    @OnClick(R.id.butter_btnUnCheckAll)
    public void onViewClicked() {
        ButterKnife.apply(countries, CHECKED, false);
    }

    @OnTextChanged(R.id.butter_edtName)
    void textChange() {
        mTextView.setText(mEditText.getText().toString());
    }

    @OnClick(R.id.butter_btnFade)
    public void onViewClicked2() {
        ButterKnife.apply(countries, View.ALPHA, 0.5f);
    }
}
