>Important learning sites.
https://mindorks.com

Animation with lottie
=====================
Example of how to use lottie json animation file.

ButterKnief
===========

https://medium.com/@someshkumar049/butterknife-right-way-1f502584a73
https://medium.com/@someshkumar049/butterknife-the-right-way-part-2-34a838fff1d6
https://medium.com/@pranaypatel/butterknife-a-viewbinding-library-for-android-beginner-guide-fd92caf8e505

RxJava, RxAndroid
========

https://medium.com/@kurtisnusbaum/rxandroid-basics-part-1-c0d5edcf6850
https://medium.com/@kurtisnusbaum/rxandroid-basics-part-2-6e877af352

https://medium.com/@kevalpatel2106/code-your-next-android-app-using-rxjava-d1db30ac9fcc
https://medium.com/@kevalpatel2106/what-is-reactive-programming-da37c1611382
https://android.jlelse.eu/what-should-you-know-about-rx-operators-54716a16b310

MVP
===
Dashboard activity is without mvp base classes and profile activity is example of base mvp classes.
Just toggle the intent of profile and dashboard where it uses.


>how to format MD file.
https://github.com/fletcher/MultiMarkdown/blob/master/Documentation/Markdown%20Syntax.md